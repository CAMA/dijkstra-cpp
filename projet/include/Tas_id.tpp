#include "Tas_id.hpp"

template <class T>
unsigned int Tas_id<T>::plusPetitIdentifiantDisponible() {
  unsigned int plusPetitIdentifiant = 1;
  if (noeuds.size() == 0)
    return plusPetitIdentifiant;
  else {
    for (typename std::vector < couple<T> >::iterator it = noeuds.begin(); it != noeuds.end(); it++) {
      if ((*it).identifiant == plusPetitIdentifiant)
        plusPetitIdentifiant++;
    }
  }
  return plusPetitIdentifiant;
}

template <class T>
couple<T> Tas_id<T>::getRacine() const {
  if (noeuds.size() != 0)
    return noeuds[0];
}

template <class T>
void Tas_id<T>::ajouterNoeud(const T &noeud) {
  couple<T> couple;
  couple.identifiant = plusPetitIdentifiantDisponible();
  couple.valeur = noeud;

  if (noeuds.size() == 0) {
    noeuds.push_back(couple);
    indiceDerniereRacine = 0;
    nbUtilisations = 0;
  }
  else {
    if (nbUtilisations == 2) {
      indiceDerniereRacine++;
      nbUtilisations = 0;
    }

    if (noeud > noeuds[indiceDerniereRacine].valeur) {
      noeuds.push_back(couple);
      nbUtilisations++;
    }
  }
}

template <class T>
couple<T> Tas_id<T>::rechercheNoeud(const T &noeud) {
  for (size_t i = 0; i < noeuds.size(); i++) {
    if (noeud == noeuds[i]) {
      cout << "Trouvé : " << noeud << endl;
      return noeuds[i];
    }
  }
  cout << "Non trouvé : " << noeud << endl;
  return NULL;
}

template <class T>
std::vector< couple<T> > Tas_id<T>::getNoeuds() {
  return noeuds;
}

template <class T>
void Tas_id<T>::supprimerRacine() {
  noeuds.erase(noeuds.begin());
  typename std::vector< couple<T> >::iterator dernierNoeud = noeuds.end();
  noeuds.pop_back();
  noeuds.insert(noeuds.begin(), *dernierNoeud);
  size_t indiceImportant = 0;
  for (size_t i = 2; i < noeuds.size(); i++) {
    if (noeuds[indiceImportant] > noeuds[i-1] && (noeuds[i-1] < noeuds[i])) {
      noeuds[indiceImportant] = noeuds[i-1];
      noeuds[i-1] = *dernierNoeud;
      indiceImportant = i-1;
      i++;
    }
  }
}

template <class T>
std::ostream &operator << (std::ostream &os, Tas_id<T> &tas_id) {
  os << "{ ";
  for (size_t i = 0; i < tas_id.getNoeuds().size(); i++) {
    os << tas_id.getNoeuds()[i] << " ";
  }
  os << "}" << std::endl;

  return os;
}
