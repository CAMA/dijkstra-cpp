#include "Tas.hpp"


template <class T>
T Tas<T>::getRacine() const {
  if (noeuds.size() != 0)
    return noeuds[0];
}

template <class T>
void Tas<T>::ajouterNoeud(const T &noeud) {
  if (noeuds.size() == 0) {
    noeuds.push_back(noeud);
    indiceDerniereRacine = 0;
    nbUtilisations = 0;
  }
  else {
    if (nbUtilisations == 2) {
      indiceDerniereRacine++;
      nbUtilisations = 0;
    }

    if (noeud > noeuds[indiceDerniereRacine]) {
      noeuds.push_back(noeud);
      nbUtilisations++;
    }
  }
}

template <class T>
void Tas<T>::supprimerRacine() {
  noeuds.erase(noeuds.begin());
  typename std::vector<T>::iterator dernierNoeud = noeuds.end();
  noeuds.pop_back();
  noeuds.insert(noeuds.begin(), *dernierNoeud);
  size_t indiceImportant = 0;
  for (size_t i = 2; i < noeuds.size(); i++) {
    if (noeuds[indiceImportant] > noeuds[i-1] && (noeuds[i-1] < noeuds[i])) {
      noeuds[indiceImportant] = noeuds[i-1];
      noeuds[i-1] = *dernierNoeud;
      indiceImportant = i-1;
      i++;
    }
  }
}

template <class T>
T* Tas<T>::rechercheNoeud(const T &noeud) {
  for (size_t i = 0; i < noeuds.size(); i++) {
    if (noeud == noeuds[i]) {
      cout << "Trouvé : " << noeud << endl;
      return &noeuds[i];
    }
  }
  cout << "Non trouvé : " << noeud << endl;
  return NULL;
}

template <class T>
std::vector<T> Tas<T>::getNoeuds() {
  return noeuds;
}

template <class T>
std::ostream &operator << (std::ostream &os, Tas<T> &tas) {
  os << "{ ";
  for (size_t i = 0; i < tas.getNoeuds().size(); i++) {
    os << tas.getNoeuds()[i] << " ";
  }
  os << "}" << std::endl;

  return os;
}
