#ifndef SOMMET_HPP
#define SOMMET_HPP

#include <set>
#include <ostream>
using namespace std;

template <class TS, class TP>
class Sommet {
private:
  set<pair<Sommet<TS,TP>*,TP> > neighbours ;
  TS etiquette;

public:
  Sommet() {}
  explicit Sommet(TS etiquette);
  void add_Sommet(Sommet<TS,TP> *sommet, TP poids);
  pair<Sommet<TS,TP>*,TP> find(Sommet<TS,TP> *sommet);
  void suppr_Sommet(Sommet<TS,TP> *sommet);
  void suppr_Sommet();
  const set<pair<Sommet<TS,TP> *, TP> > &getNeighbours() const;
  TS getEtiquette() const;
  ~Sommet(){}

  template <class TSO, class TPO>
  friend std::ostream &operator<< (std::ostream &os, const Sommet<TSO,TPO> *sommet);

  bool operator> (const Sommet<TS,TP> *s2);

  bool operator< (const Sommet<TS,TP> *s2);

  bool operator== (const Sommet<TS,TP> *s2);
};

#include "Sommet.tpp"

#endif
