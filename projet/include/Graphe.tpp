#include <iostream>
#include "Graphe.hpp"

template <class TS, class TP>
std::ostream &operator<<(std::ostream &os, const Graphe<TS,TP> &graphe) {
  os << "graph graphname { " << endl;
  for (typename set<Sommet<TS,TP>*>::iterator i = graphe.sommets.begin(); i != graphe.sommets.end(); i++) {
    os << *i ;
  }
  os << "}";
  return os;
}

template <class TS, class TP>
void Graphe<TS,TP>::ajouterSommet(Sommet<TS,TP> *s1) {
  this->sommets.insert(s1);
}

template <class TS, class TP>
set<Sommet<TS,TP>*> Graphe<TS,TP>::getSommets() {
  return sommets;
}

template <class TS, class TP>
void Graphe<TS,TP>::lierDeuxSommets(Sommet<TS,TP> *s1,Sommet<TS,TP> *s2, TP poids) {
  s1->add_Sommet(s2,poids);
  s2->add_Sommet(s1,poids);
}

template <class TS, class TP>
void Graphe<TS,TP>::supprimerSommet(Sommet<TS,TP> *s1) {
  s1->suppr_Sommet();
  delete s1;
}

template <class TS, class TP>
bool Graphe<TS,TP>::estPresent(set<TS> st, TS s) {
  for (typename set<int>::iterator i = st.begin(); i != st.end() ; ++i) {
    if(*i == s)
    return true;
  }
  return false;
}
