#include <map>
#include "Sommet.hpp"

template <class TS, class TP>
std::ostream &operator<< (std::ostream &os, const Sommet<TS,TP> *sommet) {
  if (sommet->getNeighbours().size() != 0) {
    for (typename set<pair<Sommet<TS,TP>*,TP> >::iterator i = sommet->getNeighbours().begin(); i != sommet->getNeighbours().end(); ++i) {
      if(i->first->getEtiquette() >= sommet->getEtiquette())
        os << sommet->getEtiquette() << " -- " << i->first->getEtiquette() << " (" << i->second << ")" << endl;
    }
  }
  else {
    os << sommet->getEtiquette();
  }
  return os;
}

template <class TS, class TP>
bool Sommet<TS,TP>::operator> (const Sommet<TS,TP> *s2) {
  return getEtiquette() > s2->getEtiquette();
}

template <class TS, class TP>
bool Sommet<TS,TP>::operator< (const Sommet<TS,TP> *s2) {
  return getEtiquette() < s2->getEtiquette();
}

template <class TS, class TP>
bool Sommet<TS,TP>::operator== (const Sommet<TS,TP> *s2) {
  return getEtiquette() == s2->getEtiquette();
}

template <class TS, class TP>
const set<pair<Sommet<TS,TP> *, TP> > &Sommet<TS,TP>::getNeighbours() const {
  return neighbours;
}

template <class TS, class TP>
TS Sommet<TS,TP>::getEtiquette() const {
  return etiquette;
}

template <class TS, class TP>
Sommet<TS,TP>::Sommet(TS etiquette) {
  this->etiquette = etiquette;
}

template <class TS, class TP>
void Sommet<TS,TP>::add_Sommet(Sommet<TS,TP> *sommet, TP poids) {
  pair<Sommet<TS,TP>*,TP> som = pair<Sommet<TS,TP>*,TP>();
  som.first = sommet;
  som.second = poids;
  this->neighbours.insert(som);
}

template <class TS, class TP>
void Sommet<TS,TP>::suppr_Sommet(Sommet<TS,TP> *sommet) {
  pair<Sommet<TS,TP>*,TP> sommet_p = this->find(sommet);
  this->neighbours.erase(sommet_p);
}

template <class TS, class TP>
void Sommet<TS,TP>::suppr_Sommet() {
  for(typename set<pair<Sommet<TS,TP>*,TP> >::iterator it = this->neighbours.begin(); it !=neighbours.end();++it){
    it->first->suppr_Sommet(this);
  }
  this->neighbours.clear();
}

template <class TS, class TP>
pair<Sommet<TS,TP>*, TP> Sommet<TS,TP>::find(Sommet<TS,TP> *sommet) {
  pair<Sommet<TS,TP>*,TP> sommet_p = pair<Sommet<TS,TP>*,TP>();
  for (typename set<pair<Sommet<TS,TP>*,TP> >::iterator it = this->neighbours.begin(); it != this->neighbours.end(); it++){
    if (it->first->getEtiquette() == sommet->getEtiquette()){
      sommet_p.first = sommet;
      sommet_p.second = it->second;
    }
  }
  return sommet_p;
}
