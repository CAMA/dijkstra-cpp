#ifndef TAS_HPP
#define TAS_HPP

#include <vector>
#include <string>

template <class T>
class Tas {
private:
  std::vector<T> noeuds;
  size_t indiceDerniereRacine;
  unsigned int nbUtilisations;

public:
  Tas() {}
  T getRacine() const;
  void ajouterNoeud(const T &noeud);
  T* rechercheNoeud(const T &noeud);
  void supprimerRacine();
  std::vector<T> getNoeuds();
  ~Tas() {}

  template <class TO>
  friend std::ostream &operator<< (std::ostream &os, Tas<TO> &tas);
};

#include "Tas.tpp"
#endif
