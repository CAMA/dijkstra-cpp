#ifndef TAS_ID_HPP
#define TAS_ID_HPP

#include <vector>
#include <string>

template <class T>
struct couple {
  unsigned int identifiant;
  T valeur;

  template <class TO>
  friend std::ostream &operator<< (std::ostream &os, const couple<TO> &couple) {
    os << "(" << couple.identifiant << "," << couple.valeur << ")";
  }

  bool operator > (couple<T> &c) {
    return valeur > c.valeur;
  }

  bool operator < (couple<T> &c) {
    return valeur < c.valeur;
  }

  bool operator == (couple<T> &c) {
    return valeur == c.valeur && identifiant == c.identifiant;
  }

};

template <class T>
class Tas_id {
private:
  std::vector< couple<T> > noeuds;
  size_t indiceDerniereRacine;
  unsigned int nbUtilisations;

  unsigned int plusPetitIdentifiantDisponible();

public:
  Tas_id() {}
  couple<T> getRacine() const;
  void ajouterNoeud(const T &noeud);
  couple<T> rechercheNoeud(const T &noeud);
  void supprimerRacine();
  std::vector< couple<T> > getNoeuds();
  ~Tas_id() {}

  template <class TO>
  friend std::ostream &operator<< (std::ostream &os, Tas_id<TO> &tas_id);
};

#include "Tas_id.tpp"
#endif
