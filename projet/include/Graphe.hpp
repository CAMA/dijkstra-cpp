#ifndef GRAPHE_H
#define GRAPHE_H

#include <set>
#include <ostream>
#include "Sommet.hpp"

template <class TS, class TP>
class Graphe {
private:
  set<Sommet<TS,TP>*> sommets;
public:
  Graphe() {};
  void ajouterSommet(Sommet<TS,TP>* s1);
  bool estPresent(set<TS> set, TS s);
  void lierDeuxSommets(Sommet<TS,TP> *s1, Sommet<TS,TP> *s2, TP poids);
  set<Sommet<TS,TP>*> getSommets();
  void supprimerSommet(Sommet<TS,TP> *s1);

  template <class TSO, class TPO>
  friend std::ostream &operator<< (std::ostream &os, const Graphe<TSO,TPO> &graphe);
  ~Graphe() {
    for (typename set<Sommet<TS,TP>*>::iterator it = sommets.begin(); it != sommets.end(); it++) {
      supprimerSommet(*it);
    }
  };
};

#include "Graphe.tpp"
#endif
