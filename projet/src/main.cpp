#include <iostream>
#include "Sommet.hpp"
#include "Graphe.hpp"
#include "Tas.hpp"
#include "Tas_id.hpp"
#include <string>

int main() {
  std::cout << "====================== Graphe ======================" << std::endl;

  Graphe<int,int> g = Graphe<int,int>();
  Sommet<int,int> *s1 = new Sommet<int,int>(1);
  Sommet<int,int> *s2 = new Sommet<int,int>(2);
  Sommet<int,int> *s3 = new Sommet<int,int>(3);
  Sommet<int,int> *s4 = new Sommet<int,int>(4);
  Sommet<int,int> *s5 = new Sommet<int,int>(5);
  Sommet<int,int> *s6 = new Sommet<int,int>(6);

  g.ajouterSommet(s1);
  g.ajouterSommet(s2);
  g.ajouterSommet(s3);
  g.ajouterSommet(s4);
  g.ajouterSommet(s5);
  g.ajouterSommet(s6);

  g.lierDeuxSommets(s1,s2,1);

  g.lierDeuxSommets(s2,s3,1);
  g.lierDeuxSommets(s2,s4,1);

  g.lierDeuxSommets(s3,s4,1);
  g.lierDeuxSommets(s3,s5,1);
  g.lierDeuxSommets(s3,s6,1);

  g.lierDeuxSommets(s5,s6,1);

  std::cout << g << std::endl << std::endl;

  std::cout << "====================== Tas<int> ======================" << std::endl;
  Tas<int> tas = Tas<int>();

  /*
  Tas représenté :
          1
        /  \
       2   3
     / \  / \
    4  5 6  7
  */
  tas.ajouterNoeud(1);
  tas.ajouterNoeud(2);
  tas.ajouterNoeud(3);
  tas.ajouterNoeud(4);
  tas.ajouterNoeud(5);
  tas.ajouterNoeud(2); // ne doit pas être ajouté car inférieur au père
  tas.ajouterNoeud(6);
  tas.ajouterNoeud(7);

  tas.rechercheNoeud(42);
  tas.rechercheNoeud(3);

  std::cout << "Racine actuelle : " << tas.getRacine() << std::endl;
  std::cout << tas << std::endl;

  std::cout << "Suppression de la racine" << std::endl << std::endl;
  tas.supprimerRacine();

  std::cout << "Racine actuelle : " << tas.getRacine() << std::endl;
  std::cout << tas << std::endl;

  std::cout << "====================== Tas_id<int> ======================" << std::endl;
  Tas_id<int> tas_id = Tas_id<int>();

  /*
  Tas représenté :
          1
        /  \
       2   3
     / \  / \
    4  5 6  7
  */

  tas_id.ajouterNoeud(1);
  tas_id.ajouterNoeud(2);
  tas_id.ajouterNoeud(3);
  tas_id.ajouterNoeud(4);
  tas_id.ajouterNoeud(5);
  tas_id.ajouterNoeud(2); // ne doit pas être ajouté car inférieur au père
  tas_id.ajouterNoeud(6);
  tas_id.ajouterNoeud(7);

  std::cout << "Racine actuelle : " << tas_id.getRacine() << std::endl;
  std::cout << tas_id << std::endl;

  std::cout << "Suppression de la racine" << std::endl << std::endl;
  tas_id.supprimerRacine();

  std::cout << "Racine actuelle : " << tas_id.getRacine() << std::endl;
  std::cout << tas_id << std::endl;

  return 0;
}
